﻿using NHibernate.Mapping.ByCode;
using NHibernate.SimpleMapping.Tests.Classes;
using NHibernate.SimpleMapping.Tests.Entities;
using System.Linq;
using Xunit;

namespace NHibernate.SimpleMapping.Tests
{
    public class MappingsTests
    {
        [Fact]
        public void ScanAssemblyTest()
        {
            var mappings = MapScanner.Scan(typeof(BaseEntity).Assembly);
            // This numebr will always change depends how many entities we have in this assembly.
            Assert.Equal(1, mappings.Count());
        }

        [Fact]
        public void SimpleEntityMapTest()
        {
            var entityMap = MapScanner.MapEntity(typeof(SampleEntity), useAssignedKeyGen: false);
            var mapper = new ModelMapper();
            mapper.AddMapping(entityMap);
            var mapping = mapper.CompileMappingForAllExplicitlyAddedEntities();
            var nbmClass = mapping.RootClasses.FirstOrDefault();
            Assert.NotNull(nbmClass);
            Assert.Equal("SampleEntity", nbmClass.Name);
            Assert.NotNull(nbmClass.Id);
            Assert.NotNull(nbmClass.Version);
            Assert.Null(nbmClass.Timestamp);
            Assert.Empty(nbmClass.Joins);
            Assert.Empty(nbmClass.Subclasses);
            Assert.Empty(nbmClass.JoinedSubclasses);
            Assert.Empty(nbmClass.UnionSubclasses);
            Assert.Collection(nbmClass.Properties,
                x => Assert.Equal("Name", x.Name),
                x => Assert.Equal("CreatedBy", x.Name),
                x => Assert.Equal("UpdatedBy", x.Name),
                x => Assert.Equal("CreatedDate", x.Name),
                x => Assert.Equal("UpdatedDate", x.Name),
                x => Assert.Equal("IsDeleted", x.Name)
                );
        }

        [Fact]
        public void OtherEntityMapTest()
        {
            // Needs more tests. We are always happy with any help and contribution.
        }
    }
}
