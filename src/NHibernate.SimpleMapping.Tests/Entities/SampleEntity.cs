﻿using NHibernate.SimpleMapping.Attributes;
using NHibernate.SimpleMapping.Interfaces;
using NHibernate.SimpleMapping.Tests.Classes;
using System;

namespace NHibernate.SimpleMapping.Tests.Entities
{
    [Table("samle_table")]
    public class SampleEntity : BaseEntity, IHaveName
    {
        protected SampleEntity()
        {
        }

        public SampleEntity(string name)
        {
            SetName(name);
        }

        public virtual string Name { get; protected set; }

        public virtual void SetName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException(nameof(name));

            Name = name;
        }
    }
}
