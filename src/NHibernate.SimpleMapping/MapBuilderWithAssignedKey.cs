﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace NHibernate.SimpleMapping
{
    internal class MapBuilderWithAssignedKey<TEntity> : ClassMapping<TEntity> where TEntity : class
    {
        public MapBuilderWithAssignedKey()
        {
            this.MapClass(useAssignedKeyGen: true);
        }
    }
}
