﻿using NHibernate.Engine;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace NHibernate.SimpleMapping.Types
{
    public class StringListUserType<TList, TElement> : IUserType where TList : IEnumerable<TElement>
    {
        private const char separator = ';';

        private readonly System.Type _baseType;
        private readonly bool _isEnum;
        private readonly bool _isString;
        private readonly bool _isList;
        private readonly bool _isSet;
        private readonly bool _isAnsi;

        public StringListUserType()
        {
            _baseType = typeof(TElement);
            _isEnum = _baseType.IsEnum;
            _isString = _baseType == typeof(string);
            _isList = FuncHelper.IsGenericTypeOf(typeof(TList), typeof(IList<>));
            _isSet = FuncHelper.IsGenericTypeOf(typeof(TList), typeof(ISet<>));

            if (!_isEnum && !_isString)
                throw new ArgumentException($"StringListUserType: The type {typeof(TElement).Name} cannot be an argument of {typeof(TList).Name}. Only Enum or String allowed.");
        }

        public StringListUserType(bool isAnsi) : this()
        {
            _isAnsi = isAnsi;
        }

        #region Equals member

        bool IUserType.Equals(object x, object y)
        {
            if (x == null || y == null) return false;
            var xl = (TList)x;
            var yl = (TList)y;
            if (xl.Count() != yl.Count()) return false;
            Boolean retvalue = xl.Except(yl).Count() == 0;
            return retvalue;
        }

        #endregion Equals member

        #region IUserType Members

        public object Assemble(object cached, object owner)
        {
            return cached;
        }

        public object DeepCopy(object value)
        {
            if (value == null)
                return null;

            var obj = (TList)value;

            IEnumerable<TElement> result = default;

            if (_isList)
                result = new List<TElement>(obj);
            else if (_isSet)
                result = new HashSet<TElement>(obj);
            else
                result = obj.ToArray();

            return result;
        }

        public object Disassemble(object value)
        {
            return value;
        }

        public int GetHashCode(object x)
        {
            return x.GetHashCode();
        }

        public bool IsMutable
        {
            get { return true; }
        }

        public object Replace(object original, object target, object owner)
        {
            return original;
        }

        public object NullSafeGet(DbDataReader rs, string[] names, ISessionImplementor session, object owner)
        {
            var result = new List<TElement>();

            Int32 index = rs.GetOrdinal(names[0]);

            if (!rs.IsDBNull(index) && !string.IsNullOrEmpty((string)rs[index]))
            {
                var elements = ((string)rs[index]).Split(new char[] { separator });

                foreach (object s in elements)
                {
                    if (_isEnum && FuncHelper.TryParseEnum<TElement>(s.ToString(), out var value))
                        result.Add(value);
                    else if (_isString)
                        result.Add((TElement)s);
                }
            }

            if (_isList)
                return result;
            else if (_isSet)
                return new HashSet<TElement>(result);
            else
                return result.ToArray();
        }

        public void NullSafeSet(DbCommand cmd, object value, int index, ISessionImplementor session)
        {
            if (value == null || value == DBNull.Value)
            {
                if (_isAnsi)
                    NHibernateUtil.AnsiString.NullSafeSet(cmd, null, index, session);
                else
                    NHibernateUtil.String.NullSafeSet(cmd, null, index, session);
            }
            else
            {
                var sb = string.Join(separator.ToString(), (TList)value);
                if (_isAnsi)
                    NHibernateUtil.String.Set(cmd, sb, index, session);
                else
                    NHibernateUtil.String.Set(cmd, sb, index, session);
            }
        }

        public System.Type ReturnedType
        {
            get { return typeof(TList); }
        }

        public NHibernate.SqlTypes.SqlType[] SqlTypes
        {
            get { return new SqlType[] { _isAnsi ? NHibernateUtil.AnsiString.SqlType : NHibernateUtil.String.SqlType }; }
        }

        #endregion IUserType Members
    }

}
