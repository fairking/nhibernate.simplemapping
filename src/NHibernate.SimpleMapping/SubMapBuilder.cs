﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace NHibernate.SimpleMapping
{
    internal class SubMapBuilder<TEntity> : SubclassMapping<TEntity> where TEntity : class
    {
        public SubMapBuilder()
        {
            this.MapSubClass();
        }
    }
}
