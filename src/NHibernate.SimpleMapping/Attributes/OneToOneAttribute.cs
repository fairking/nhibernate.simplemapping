﻿using System;

namespace NHibernate.SimpleMapping.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class OneToOneAttribute : Attribute
    {
        public OneToOneAttribute() : base() { }

        public OneToOneAttribute(string otherSidePropertyName) : base()
        {
            OtherSidePropertyName = otherSidePropertyName;
        }

        /// <summary>
        /// Used for OneToOne relation. There must be ManyToOne property one the other side with [Unique] attribute.
        /// If property name is not given it finds single property with the same type.
        /// </summary>
        public string OtherSidePropertyName { get; set; }
    }
}
