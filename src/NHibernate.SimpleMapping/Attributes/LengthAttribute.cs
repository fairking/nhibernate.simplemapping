﻿using System;

namespace NHibernate.SimpleMapping.Attributes
{
    /// <summary>
    /// Max length of the string
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class LengthAttribute : Attribute
    {
        public int Max { get; protected set; }

        public LengthAttribute(int maxLength)
        {
            Max = maxLength;
        }
    }
}
