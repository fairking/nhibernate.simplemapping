﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NHibernate.SimpleMapping.Attributes
{
    public class UtcDateTimeAttribute : Attribute
    {
        public bool NoMs { get; set; }
    }
}
