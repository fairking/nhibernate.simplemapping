﻿using System;

namespace NHibernate.SimpleMapping.Attributes
{
    /// <summary>
    /// Property is being encrypted by IEncryptor instance. Only used against string properties.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class EncryptedAttribute : Attribute
    {

    }
}
