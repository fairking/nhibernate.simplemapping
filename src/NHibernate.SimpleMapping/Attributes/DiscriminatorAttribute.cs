﻿using System;

namespace NHibernate.SimpleMapping.Attributes
{
    /// <summary>
    /// Class discriminator attribute applies to the base class. You can also apply to a readonly property.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property)]
    public class DiscriminatorAttribute : Attribute
    {
        public DiscriminatorAttribute()
        { }

        public DiscriminatorAttribute(string columnName)
        {
            ColumnName = columnName;
        }

        public string ColumnName { get; protected set; }
    }
}
