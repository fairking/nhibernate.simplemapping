﻿using NHibernate.Mapping.ByCode;
using System;
using System.Collections.Generic;
using System.Text;

namespace NHibernate.SimpleMapping.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class CascadeAttribute : Attribute
    {
        public CascadeAttribute(Cascade cascade)
        {
            Cascade = cascade;
        }

        public Cascade Cascade { get; protected set; }
    }
}
