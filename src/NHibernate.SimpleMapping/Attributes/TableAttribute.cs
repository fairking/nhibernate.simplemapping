﻿using System;

namespace NHibernate.SimpleMapping.Attributes
{
    /// <summary>
    /// Every entity should have this attribute to be able to map or be a reference. Add [NotMapped] if you'd like to map
    /// entity in traditional way (hbm.xml or bycode). This attribute also used in many-to-many relations against property.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property)]
    public class TableAttribute : Attribute
    {
        public TableAttribute()
        { }

        public TableAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; protected set; }
        public string Schema { get; set; }
        /// <summary>
        /// Key column name for the joined subclass mapping. Default: {BaseClassName}+Id
        /// </summary>
        public string JoinedKey { get; set; }
    }
}