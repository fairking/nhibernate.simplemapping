﻿using System;

namespace NHibernate.SimpleMapping.Attributes
{
    /// <summary>
    /// The property cannot be null
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class NotNullAttribute : Attribute
    {
    }
}
