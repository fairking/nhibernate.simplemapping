﻿using System;

namespace NHibernate.SimpleMapping.Attributes
{
    /// <summary>
    /// The attribute against class won't map an entity, or against propery won't map a column. It is not applied to inherited class but it is applied to inherited columns.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property)]
    public class NotMappedAttribute : Attribute
    {
    }
}
