﻿namespace NHibernate.SimpleMapping.Interfaces
{
    public interface IHaveName
    {
        void SetName(string name);
        string Name { get; }
    }
}
