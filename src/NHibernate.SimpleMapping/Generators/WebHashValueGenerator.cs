﻿using NHibernate.Engine;
using NHibernate.Id;
using NHibernate.Mapping.ByCode;
using NHibernate.SimpleMapping.Interfaces;
using System.Threading;
using System.Threading.Tasks;

namespace NHibernate.SimpleMapping.Generators
{
    public class WebHashValueGenerator : IIdentifierGenerator
    {
        public object Generate(ISessionImplementor session, object obj)
        {
            if (obj is IEntity)
                return (obj as IEntity).Id ?? IdentityGenerator.WebHash();
            else
                return IdentityGenerator.WebHash();
        }

        public Task<object> GenerateAsync(ISessionImplementor session, object obj, CancellationToken cancellationToken)
        {
            return Task.Run(() => Generate(session, obj), cancellationToken);
        }
    }

    public class WebHashValueGeneratorDef : IGeneratorDef
    {
        public string Class => typeof(WebHashValueGenerator).AssemblyQualifiedName;

        public object Params => null;

        public System.Type DefaultReturnType => typeof(string);

        public bool SupportedAsCollectionElementId => true;
    }
}
