﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace NHibernate.SimpleMapping
{
    internal class SubMapJoinedBuilder<TEntity> : SubclassMapping<TEntity> where TEntity : class
    {
        public SubMapJoinedBuilder()
        {
            this.MapSubJoinedClass();
        }
    }
}
